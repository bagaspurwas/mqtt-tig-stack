#!/bin/bash

piopath=$(pwd)/hardware
srcpath=$piopath/src
pio=$(which platformio)

if [ $# -ne 2 ]; then
    echo "Usage:"
    echo "./configHardware.sh <hospital> <patient>"
    exit
else
    echo "Modifying config with following topic: mqtt/$1/id/$2/bpm"
    sed -n "s/topic = \"\"/topic = \"mqtt\/$1\/id\/$2\/bpm\"/" $srcpath/main.cpp.temp > $srcpath/main.cpp
fi

# Build source code and upload
# Build and upload specificatio is defined in platformio.ini
cd $piopath
$pio run --target clean
$pio run -t upload



