#!/bin/bash
data=$(awk -v min=65 -v max=70 'BEGIN{srand(); print int(min+rand()*(max-min+1))}')
certpath=$(pwd)/openssl
#echo "Using cert path $certpath"
#echo "Sending data securely: $data"
#mosquitto_pub -h mqtt -p 8883 -u ltka -P ltka1337 --insecure --cafile $certpath/ca.crt --cert $certpath/client2.crt --key $certpath/client2.key -t mqtt/RS_XYZ/id/pasienx/bpm -m $data
for (( c=1; c<=30; c++ ))
do
    data=$(awk -v min=69 -v max=72 'BEGIN{srand(); print int(min+rand()*(max-min+1))}')
    echo "Sending data: $data"
    mosquitto_pub -h ltka.localhost -p 1883 -u ltka -P ltka1337 -t mqtt/RS_XYZ/id/pasienx/bpm -m $data
    data=$(awk -v min=60 -v max=66 'BEGIN{srand(); print int(min+rand()*(max-min+1))}')
    echo "Sending data: $data"
    mosquitto_pub -h ltka.localhost -p 1883 -u ltka -P ltka1337 -t mqtt/RS_XYZ/id/pasieny/bpm -m $data
    data=$(awk -v min=64 -v max=66 'BEGIN{srand(); print int(min+rand()*(max-min+1))}')
    echo "Sending data: $data"
    mosquitto_pub -h ltka.localhost -p 1883 -u ltka -P ltka1337 -t mqtt/RS_XYZ/id/pasienz/bpm -m $data
    data=$(awk -v min=63 -v max=66 'BEGIN{srand(); print int(min+rand()*(max-min+1))}')
    echo "Sending data: $data"
    mosquitto_pub -h ltka.localhost -p 1883 -u ltka -P ltka1337 -t mqtt/RS_XYZ/id/pasiena/bpm -m $data
    sleep 1
done
