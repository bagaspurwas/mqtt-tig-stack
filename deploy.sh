#!/bin/bash
export rut=`pwd`
docker-compose up -d
sleep 2
curl -iG -XPOST http://127.0.0.1:8086/query --data-urlencode "q=CREATE DATABASE telegraf"
curl -iG -XPOST http://127.0.0.1:8086/query --data-urlencode "q=CREATE USER telegraf WITH PASSWORD 'telegraf1337'"
curl -iG -XPOST http://127.0.0.1:8086/query --data-urlencode "q=GRANT ALL ON telegraf TO telegraf"

