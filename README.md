# Visualize MQTT Data

Store MQTT data and visualize in Grafana.
Telegraf is used as interface from mosquitto mqtt server to influxdb data format

### Prerequisites
```
docker
docker-compose
```
### How to deploy?
```
clone this repo
cd mqtt-tig-stack
docker-compose up -d
```